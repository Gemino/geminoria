var interval, video = document.getElementsByTagName("video")[0], 
stage = document.getElementById("stage"),
canvas = document.createElement("canvas"),
captureImage = function () {
                var ctx;
                canvas.width = video.videoWidth;
                canvas.height = video.videoHeight;
                if (canvas.width) {
                    ctx = canvas.getContext('2d');
                    ctx.clearRect(0, 0, canvas.width, canvas.height);
                    ctx.drawImage(video, 0, 0, canvas.width, canvas.height);
                    stage.innerText = toChars(ctx, canvas.width, canvas.height, canvas.width/3);
                }
            },
            beginCapture = function () {
                endCapture();
                interval = setInterval(function () {
                    captureImage(1)
                }, 100);
            },
            endCapture = function () {
                if (interval) {
                    clearInterval(interval);
                }
            },
            play = function () {
                var file = document.getElementById('file').files[0];
                var url = URL.createObjectURL(file);
                if (!file) {
                    alert("请先选择文件");
                }
                console.log(url);
                video.src = url;
                video.play();
            };
    video.addEventListener("play", beginCapture);
    video.addEventListener("pause", endCapture);
    video.addEventListener("ended", endCapture);
    video.addEventListener("playing", beginCapture);
