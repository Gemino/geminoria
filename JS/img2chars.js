/* @Auther E.C.Ares */

var map = getCharsMap();

function toChars(context, width, height, rowChars) {
    var pixels = [],
        output = "",
        imageData = context.getImageData(0, 0, width, height),
        rowChars = width < rowChars ? width : rowChars,
        char_w = width / rowChars,
        char_h = char_w * 3,
        rows = height / char_h,
        cols = rowChars,

        getBlockGray = function (x, y, w, h) {
            var sumGray = 0, pixels;
            for (var row = 0; row < h; row++) {
                for (var col = 0; col < w; col++) {
                    var cx = x + col, //current position x
                        cy = y + row, //current positon y
                        index = (cy * imageData.width + cx) * 4, //current index in rgba data array
                        data = imageData.data,
                        R = data[index],
                        G = data[index + 1],
                        B = data[index + 2],
                        gray = ~~(R * 0.3 + G * 0.59 + B * 0.11);
                    sumGray += gray;
                }
            }
            pixels = w * h;
            return ~~(sumGray / pixels);
        };

    for (var r = 0; r < rows; r++) {
        for (var c = 0; c < cols; c++) {
            var pos_x = ~~(c * char_w),
                pos_y = ~~(r * char_h),
                avg = getBlockGray(pos_x, pos_y, ~~char_w, ~~char_h),
                ch = map[avg];
            output += ch;
        }
        output += '\r\n';
    }
    return output;
}

function getCharsMap() {
    var chars = ['@', '$', '&', '#', 'M', 'X', 't', 'd', 'O', '7', 'l', 'j', 'i', ';', '.', ' '];
    var step = 6,
        map = {};
    for (var i = 0; i < 256; i++) {
        var index = ~~(i / 16);
        map[i] = chars[index];
    }
    ;
    return map;
}